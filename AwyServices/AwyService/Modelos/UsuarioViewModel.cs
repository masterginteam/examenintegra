﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AwyService.Modelos
{
    public class UsuarioViewModel
    {
        public long Id { get; set; }
        public string Nombre { get; set; }
        public string ApellidoMaterno { get; set; }
        public string ApellidoPaterno { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public DateTime Expira { get; set; }

    }
}
