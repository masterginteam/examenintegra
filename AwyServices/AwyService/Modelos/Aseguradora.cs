﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AwyService.Modelos
{
	public partial class Aseguradora
	{

		public Aseguradora()
		{
			ConfiguracionSeguro = new HashSet<ConfiguracionSeguro>();
		}
		public long Id { get; set; }
		public string Nombre { get; set; }
		public virtual ICollection<ConfiguracionSeguro> ConfiguracionSeguro { get; set; }
	}
}
