﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AwyService.Modelos.Request
{
    public class UsuarioRequest
    {
        public long Id { get; set; }
        public string Nombre { get; set; }
        public string ApellidoMaterno { get; set; }
        public string ApellidoPaterno { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
