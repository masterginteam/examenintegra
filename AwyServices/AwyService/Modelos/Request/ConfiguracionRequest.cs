﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AwyService.Modelos.Request
{
    public class ConfiguracionRequest
    {
        public string Vehiculo { get; set; }
        public long IdUsuario { get; set; }
        public long IdAseguradora { get; set; }

        public string NombreCliente { get; set; }
        public DetalleConfiguracion Configuracion { get; set; }
        
    }
}
