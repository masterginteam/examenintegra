﻿using System;
using System.Collections.Generic;

namespace AwyService.Modelos
{
    public partial class Usuario
    {
        public Usuario()
        {
            ConfiguracionSeguro = new HashSet<ConfiguracionSeguro>();
        }

        public long Id { get; set; }
        public string Nombre { get; set; }
        public string ApellidoMaterno { get; set; }
        public string ApellidoPaterno { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }

        public virtual ICollection<ConfiguracionSeguro> ConfiguracionSeguro { get; set; }
    }
}
