﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AwyService.Modelos.Response
{
    public class ConfiguracionResponse
    {
        public long Id { get; set; }
        public string Vehiculo { get; set; }
        public string Usuario { get; set; }
        public string Aseguradora { get; set; }
        public DetalleConfiguracion Configuracion { get; set; }

        public DateTime FechaCreacion { get; set; }
    }
}
