﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AwyService.Modelos.Response
{
    public class EncriptadoResponse
    {
        public string Salt { get; set; }
        public string Password { get; set; }
    }
}
