﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AwyService.Modelos.Automaper
{
    public class AutoMapper : Profile
    {
        public AutoMapper()
        {
            CreateMap<ConfiguracionSeguro, ConfiguracionViewModel>();
            CreateMap<ConfiguracionViewModel, ConfiguracionSeguro>();
        }
    }
}
