﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace AwyService.Modelos
{
    public partial class AwySegurosContext : DbContext
    {
        public AwySegurosContext()
        {
        }

        public AwySegurosContext(DbContextOptions<AwySegurosContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ConfiguracionSeguro> ConfiguracionSeguro { get; set; }
        public virtual DbSet<Usuario> Usuario { get; set; }
        public virtual DbSet<Aseguradora> Aseguradora { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=awyserver.database.windows.net;Database=cotizador-whatsapp;User ID=AwyAdmin;Password=AWY4dm1n2020;");
               // optionsBuilder.UseSqlServer("Server=DESKTOP-9FEOVP9;Database=AwySeguros;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ConfiguracionSeguro>(entity =>
            {
                entity.Property(e => e.Configuracion)
                      .IsUnicode(false);

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.IdUsuario).HasColumnName("Id_usuario");

                entity.Property(e => e.NombreCliente)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Vehiculo)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.ConfiguracionSeguro)
                    .HasForeignKey(d => d.IdUsuario)
                    .HasConstraintName("FK_ConfiguracionSeguro_Usuario");

                entity.HasOne(d => d.IdAseguradoraNavigation)
	                .WithMany(p => p.ConfiguracionSeguro)
	                .HasForeignKey(d => d.IdAseguradora)
	                .HasConstraintName("FK_ConfiguracionSeguro_Aseguradora");
            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.HasIndex(e => e.Email)
                    .HasName("IX_Usuario")
                    .IsUnique();

                entity.Property(e => e.ApellidoMaterno)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ApellidoPaterno)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Salt)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(70)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<Aseguradora>(entity =>
            {
	           entity.Property(e => e.Nombre)
		            .IsRequired()
		            .HasMaxLength(70)
		            .IsUnicode(false);


	          
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
