﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AwyService.Modelos
{
    public class DetalleConfiguracion
    {
        public ConceptosConfiguracion Aseguradora1 { get; set; }
        public ConceptosConfiguracion Aseguradora2 { get; set; }
        public ConceptosConfiguracion Aseguradora3 { get; set; }
    }
    public class ConceptosConfiguracion
    {
        public string Cobertura { get; set; }
        public ValoresConfiguracionString DaniosMateriales { get; set; }
        public ValoresConfiguracionString NoPagoDeduciblePerdidaParcial { get; set; }
        public ValoresConfiguracionString NoPagoDeduciblePerdidaTotal { get; set; }
        public ValoresConfiguracionString RoboTotal { get; set; }
        public ValoresConfiguracionString ResponsabilidadCivilTerceros { get; set; }
        public ValoresConfiguracionString GastosMedicosOcupantes { get; set; }
        public ValoresConfiguracionString AsistenciaVial { get; set; }
        public ValoresConfiguracionString DefensaLegal { get; set; }
        public ValoresConfiguracionString ResponsabilidadCivilFallecimiento { get; set; }
        public ValoresConfiguracionString ResponsabilidadCivilEUA { get; set; }
        public ValoresConfiguracionString ResponsabilidadCivilOcupantes { get; set; }
        public ValoresConfiguracionString MuerteAccidentaConductor { get; set; }
        public ValoresConfiguracionString AutoSustituto { get; set; }
        public ValoresConfiguracionString UltimosGastosOcupantes { get; set; }
        public ValoresConfiguracionString ExtensionCoberturas { get; set; }
        public ValoresConfiguracionString LlantasRines { get; set; }
        public ValoresConfiguracionString RoboParcial { get; set; }
        public ValoresConfiguracionString EspejosFaros { get; set; }
        public ValoresConfiguracionString MultasCorralones { get; set; }
        public ValoresConfiguracionString Desbielamiento { get; set; }
        public ValoresConfiguracionString ReparacionAgencia { get; set; }
        public ValoresConfiguracionString AsistenciaServiciosFunerarios { get; set; }
        public ValoresConfiguracionString ReposiscionLlaves { get; set; }
        public ValoresConfiguracionString AutoMasNuevo { get; set; }
        public ValoresConfiguracionString ResponsabilidadCivilFamiliar { get; set; }
        public ValoresConfiguracionString RoboContenidos { get; set; }
        public ValoresConfiguracionString EquipoEspecial { get; set; }
        public ValoresConfiguracionString ResponsabilidadCivilRemolques { get; set; }
        public ValoresConfiguracionString AdaptacionConversiones { get; set; }
        public ValoresConfiguracionString ResponsabilidadCivilAdaptacionConver { get; set; }

    }

    public class ValoresConfiguracionString {
        public bool Seleccionado { get; set; }
        public string Valor { get; set; }
    }
  
}
