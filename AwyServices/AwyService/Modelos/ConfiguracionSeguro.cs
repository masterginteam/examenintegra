﻿using System;
using System.Collections.Generic;

namespace AwyService.Modelos
{
    public partial class ConfiguracionSeguro
    {
        public long Id { get; set; }
        public string Vehiculo { get; set; }
        public string NombreCliente { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string Configuracion { get; set; }
        public long IdUsuario { get; set; }

        public long IdAseguradora { get; set; }

        public virtual Usuario IdUsuarioNavigation { get; set; }
        public virtual Aseguradora IdAseguradoraNavigation { get; set; }
    }
}
