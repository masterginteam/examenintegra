﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AwyService.Modelos;
using AwyService.Modelos.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AwyService.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class AseguradoraController : Controller
	{
		public AseguradoraController()
		{
			
		}
		[Authorize]
		[HttpGet]
		public IActionResult Get()
		{
			Response oResponse = new Response();
			try
			{
				using (AwySegurosContext db = new AwySegurosContext())
				{
					var list = db.Aseguradora.ToList();
					oResponse.Data = list;
					oResponse.Exito = 1;

				}
			}
			catch (Exception e)
			{
				oResponse.Mensaje = e.Message;
			}
			return Ok(oResponse);
		}
	}
}
