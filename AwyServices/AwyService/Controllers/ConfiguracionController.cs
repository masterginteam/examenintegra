﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AwyService.Modelos;
using AwyService.Modelos.Response;
using AwyService.Modelos.Request;
using Newtonsoft.Json;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace AwyService.Controllers
{
	[Route("api/[controller]")]
	[ApiController]

	public class ConfiguracionController : ControllerBase
	{
		private readonly IMapper _mapper;
		public ConfiguracionController(IMapper mapper)
		{
			_mapper = mapper;
		}
		[Authorize]
		[HttpGet]
		public IActionResult Get()
		{
			Response oResponse = new Response();
			try
			{
				using (AwySegurosContext db = new AwySegurosContext())
				{
					var list = db.ConfiguracionSeguro.Include(u => u.IdAseguradoraNavigation).Include(u => u.IdUsuarioNavigation).ToList().Select((data) =>
					new ConfiguracionViewModel
					{
						Id = data.Id,
						FechaCreacion = data.FechaCreacion,
						NombreCliente = data.NombreCliente,
						Vehiculo = data.Vehiculo,
						Usuario = data.IdUsuarioNavigation.Nombre + " " + data.IdUsuarioNavigation.ApellidoPaterno + " " + data.IdUsuarioNavigation.ApellidoPaterno,
						Aseguradora = data.IdAseguradoraNavigation.Nombre
					}).ToList();
					oResponse.Data = list;
					oResponse.Exito = 1;

				}
			}
			catch (Exception e)
			{
				oResponse.Mensaje = e.Message;
			}
			return Ok(oResponse);
		}
		[Authorize]
		[HttpPost("Crear")]
		public IActionResult Add(ConfiguracionRequest configuracion)
		{
			Response oResponse = new Response();
			try
			{
				using (AwySegurosContext db = new AwySegurosContext())
				{
					var oCOnfiguracion = new ConfiguracionSeguro();
					oCOnfiguracion.FechaCreacion = DateTime.Now.ToLocalTime();
					oCOnfiguracion.Vehiculo = configuracion.Vehiculo;
					oCOnfiguracion.IdUsuario = configuracion.IdUsuario;
					oCOnfiguracion.IdAseguradora = configuracion.IdAseguradora;
					oCOnfiguracion.NombreCliente = configuracion.NombreCliente;
					oCOnfiguracion.Configuracion = JsonConvert.SerializeObject(configuracion.Configuracion);
					db.ConfiguracionSeguro.Add(oCOnfiguracion);
					db.SaveChanges();
					oResponse.Data = oCOnfiguracion;
					oResponse.Exito = 1;

				}
			}
			catch (Exception e)
			{
				oResponse.Mensaje = e.Message;
			}
			return Ok(oResponse);
		}

		[HttpGet("{id}", Name = "obtenerConfiguracion")]
		public IActionResult Find(long id)
		{
			Response oResponse = new Response();
			ConfiguracionResponse oRespuesta = new ConfiguracionResponse();
			try
			{
				using (AwySegurosContext db = new AwySegurosContext())
				{
					var oCOnfiguracion = db.ConfiguracionSeguro.Where(x => x.Id == id).Include(u => u.IdAseguradoraNavigation).FirstOrDefault();
					oRespuesta.Id = oCOnfiguracion.Id;
					oRespuesta.Usuario = oCOnfiguracion.NombreCliente;
					oRespuesta.Aseguradora = oCOnfiguracion.IdAseguradoraNavigation.Nombre;
					oRespuesta.FechaCreacion = oCOnfiguracion.FechaCreacion;
					oRespuesta.Configuracion = JsonConvert.DeserializeObject<DetalleConfiguracion>(oCOnfiguracion.Configuracion);
					oRespuesta.Vehiculo = oCOnfiguracion.Vehiculo;
					oResponse.Exito = 1;
					oResponse.Data = oRespuesta;
				}
			}
			catch (Exception e)
			{
				oResponse.Mensaje = e.Message;
			}
			return Ok(oResponse);
		}
		[Authorize]
		[HttpDelete("{id}", Name = "Elminar")]
		public IActionResult Delete(long id)
		{
			Response oResponse = new Response();
			ConfiguracionResponse oRespuesta = new ConfiguracionResponse();
			try
			{
				using (AwySegurosContext db = new AwySegurosContext())
				{
					var oCOnfiguracion = db.ConfiguracionSeguro.Where(x => x.Id == id).FirstOrDefault();
					db.ConfiguracionSeguro.Remove(oCOnfiguracion);
					db.SaveChanges();
					oResponse.Exito = 1;
				}
			}
			catch (Exception e)
			{
				oResponse.Mensaje = e.Message;
			}
			return Ok(oResponse);
		}

	}
}
