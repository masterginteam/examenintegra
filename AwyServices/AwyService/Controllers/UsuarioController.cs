﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AwyService.Modelos.Response;
using Microsoft.AspNetCore.Http;
using AwyService.Modelos;
using Microsoft.AspNetCore.Mvc;
using AwyService.Modelos.Request;
using AwyService.Encriptamiento;
using System.Reflection.Metadata;
using Microsoft.AspNetCore.Authorization;

namespace AwyService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
       
        [HttpGet]
        [Authorize]
        public IActionResult  Get() {
            Response oResponse = new Response();
            try
            {
                using (AwySegurosContext db = new AwySegurosContext())
                {
                    var list = db.Usuario.ToList();
                    oResponse.Data = list;
                    oResponse.Exito = 1;

                }
            }
            catch (Exception e) {
                oResponse.Mensaje = e.Message;
            }
            return Ok(oResponse);
        }

        [HttpPost("Login")]
        public IActionResult Login(UsuarioRequest model)
        {
            Response oResponse = new Response();
            Encriptado Encriptado = new Encriptado();
            try
            {
                UsuarioViewModel respuesta = new UsuarioViewModel();
                using (AwySegurosContext db = new AwySegurosContext())
                {
                    var usuario = db.Usuario.Where(x => x.Email == model.Nombre).FirstOrDefault();
                    if(usuario!= null)
                    {
                        string password = Encriptado.DesencriptadoSalt(model.Password, usuario.Salt);
                        if(password== usuario.Password) {
                            oResponse.Exito = 1;
                            var date = DateTime.UtcNow;
                            var expireDate = TimeSpan.FromHours(5);
                            respuesta.Token = Encriptado.GenerateToken(date, usuario.Email, expireDate);
                            respuesta.Expira= date.Add(expireDate);
                            respuesta.Id = usuario.Id;
                            respuesta.Nombre = usuario.Nombre;
                            respuesta.Email = usuario.Email;
                            respuesta.ApellidoMaterno = usuario.ApellidoMaterno;
                            respuesta.ApellidoPaterno = usuario.ApellidoPaterno;
                            oResponse.Data = respuesta;
                        }                           
                        else
                            oResponse.Mensaje = "La contraseña es incorrecta";
                    }
                    else
                    oResponse.Mensaje = "No se tiene registrado el correo verifique e intenten de nuevo";
                    return Ok(oResponse);

                }
            }
            catch (Exception e)
            {
                oResponse.Mensaje = e.Message;
            }
            return Ok(oResponse);
        }

        [HttpPost]
   
        public IActionResult Add(UsuarioRequest model)
        {
            Response oResponse = new Response();
            try
            {
                Encriptado Encriptado = new Encriptado();
                EncriptadoResponse DatosEncriptado = Encriptado.EncriptadoSalt(model.Password);


                using (AwySegurosContext db = new AwySegurosContext())
                {
                    Usuario oUsuario = new Usuario();
                    oUsuario.Nombre = model.Nombre;
                    oUsuario.Email = model.Email;
                    oUsuario.ApellidoMaterno = model.ApellidoMaterno;
                    oUsuario.ApellidoPaterno = model.ApellidoPaterno;
                    oUsuario.Password = DatosEncriptado.Password;
                    oUsuario.Salt = DatosEncriptado.Salt;

                    db.Usuario.Add(oUsuario);
                    db.SaveChanges();
                    oResponse.Exito = 1;

                }
            }
            catch (Exception e)
            {
                oResponse.Mensaje = e.Message;
            }
            return Ok(oResponse);
        }
    }
}
