﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AwyService.Migrations
{
    public partial class Aseguradoras : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "IdAseguradora",
                table: "ConfiguracionSeguro",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "IdAseguradoraNavigationId",
                table: "ConfiguracionSeguro",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Aseguradora",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(unicode: false, maxLength: 70, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Aseguradora", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ConfiguracionSeguro_IdAseguradoraNavigationId",
                table: "ConfiguracionSeguro",
                column: "IdAseguradoraNavigationId");

            migrationBuilder.AddForeignKey(
                name: "FK_ConfiguracionSeguro_Aseguradora_IdAseguradoraNavigationId",
                table: "ConfiguracionSeguro",
                column: "IdAseguradoraNavigationId",
                principalTable: "Aseguradora",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ConfiguracionSeguro_Aseguradora_IdAseguradoraNavigationId",
                table: "ConfiguracionSeguro");

            migrationBuilder.DropTable(
                name: "Aseguradora");

            migrationBuilder.DropIndex(
                name: "IX_ConfiguracionSeguro_IdAseguradoraNavigationId",
                table: "ConfiguracionSeguro");

            migrationBuilder.DropColumn(
                name: "IdAseguradora",
                table: "ConfiguracionSeguro");

            migrationBuilder.DropColumn(
                name: "IdAseguradoraNavigationId",
                table: "ConfiguracionSeguro");
        }
    }
}
