﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AwyService.Migrations
{
    public partial class initialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Usuario",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(unicode: false, maxLength: 70, nullable: false),
                    ApellidoMaterno = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    ApellidoPaterno = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Email = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Password = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    Salt = table.Column<string>(unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuario", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ConfiguracionSeguro",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Vehiculo = table.Column<string>(unicode: false, maxLength: 150, nullable: true),
                    NombreCliente = table.Column<string>(unicode: false, maxLength: 150, nullable: true),
                    FechaCreacion = table.Column<DateTime>(type: "datetime", nullable: true),
                    Configuracion = table.Column<string>(unicode: false, nullable: true),
                    Id_usuario = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConfiguracionSeguro", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ConfiguracionSeguro_Usuario",
                        column: x => x.Id_usuario,
                        principalTable: "Usuario",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ConfiguracionSeguro_Id_usuario",
                table: "ConfiguracionSeguro",
                column: "Id_usuario");

            migrationBuilder.CreateIndex(
                name: "IX_Usuario",
                table: "Usuario",
                column: "Email",
                unique: true,
                filter: "[Email] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ConfiguracionSeguro");

            migrationBuilder.DropTable(
                name: "Usuario");
        }
    }
}
