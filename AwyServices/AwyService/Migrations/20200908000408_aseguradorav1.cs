﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AwyService.Migrations
{
    public partial class aseguradorav1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ConfiguracionSeguro_Aseguradora_IdAseguradoraNavigationId",
                table: "ConfiguracionSeguro");

            migrationBuilder.DropIndex(
                name: "IX_ConfiguracionSeguro_IdAseguradoraNavigationId",
                table: "ConfiguracionSeguro");

            migrationBuilder.DropColumn(
                name: "IdAseguradoraNavigationId",
                table: "ConfiguracionSeguro");

            migrationBuilder.CreateIndex(
                name: "IX_ConfiguracionSeguro_IdAseguradora",
                table: "ConfiguracionSeguro",
                column: "IdAseguradora");

            migrationBuilder.AddForeignKey(
                name: "FK_ConfiguracionSeguro_Aseguradora",
                table: "ConfiguracionSeguro",
                column: "IdAseguradora",
                principalTable: "Aseguradora",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ConfiguracionSeguro_Aseguradora",
                table: "ConfiguracionSeguro");

            migrationBuilder.DropIndex(
                name: "IX_ConfiguracionSeguro_IdAseguradora",
                table: "ConfiguracionSeguro");

            migrationBuilder.AddColumn<long>(
                name: "IdAseguradoraNavigationId",
                table: "ConfiguracionSeguro",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ConfiguracionSeguro_IdAseguradoraNavigationId",
                table: "ConfiguracionSeguro",
                column: "IdAseguradoraNavigationId");

            migrationBuilder.AddForeignKey(
                name: "FK_ConfiguracionSeguro_Aseguradora_IdAseguradoraNavigationId",
                table: "ConfiguracionSeguro",
                column: "IdAseguradoraNavigationId",
                principalTable: "Aseguradora",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
