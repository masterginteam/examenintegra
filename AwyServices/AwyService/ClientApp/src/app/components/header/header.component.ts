import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { Usuario } from 'src/app/shared/interfaces';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public app_name: string = 'AwySeguros';
  public isLogged: boolean = false;
  constructor(
    private _api:ApiService,
    private router: Router) { }

  ngOnInit() {
    this._api.usuarioSubject.subscribe((user:Usuario)=>{
      if(user){
        this.isLogged= true;
      }else{
        this.isLogged= false;
      }
    });
  }
  onLogout() {
    this._api.logout();
    this.router.navigate(['login']);
  }

}
