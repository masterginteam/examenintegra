import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiconfiguracionService } from 'src/app/services/apiconfiguracion.service';
import { ApiAseguradoraService } from 'src/app/services/api.aseguradora.service';
import { Configuracion, Aseguradora, Usuario } from 'src/app/shared/interfaces';
import { NgxSpinnerService } from "ngx-spinner";
import { ClipboardService } from 'ngx-clipboard';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { environment } from '../../../../environments/environment';
import { ApiService } from 'src/app/services/api.service';
declare var $:any;
@Component({
  selector: 'app-agregarconfiguracion',
  templateUrl: './agregarconfiguracion.component.html',
  styleUrls: ['./agregarconfiguracion.component.scss']
})
export class AgregarconfiguracionComponent implements OnInit {
  
  api_url:string = environment.urlDetalles
  constructor(private ApiConfiguracion: ApiconfiguracionService,
    private ApiAseguradora: ApiAseguradoraService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private _api:ApiService,
    private _clipboardService: ClipboardService) { }
    public detallesConfiguracion:Configuracion= new Configuracion();
    public Aseguradoras:Aseguradora [];
    
   public id:number;
  ngOnInit() {
    this.spinner.show();
    
    this._api.usuarioSubject.subscribe((user:Usuario)=>{
      if(user)
        this.id= user.id;
     
    });
    $('[data-toggle="tooltip"]').tooltip();
    this.detallesConfiguracion.idaseguradora= -1;
    this.ApiAseguradora.getAllAseguradoras().subscribe(response=>
      {
        this.spinner.hide();
        if(response.exito===1){
          this.Aseguradoras = response.data;
        }else{
          Swal.fire({
            icon: 'error',
            text: response.mensaje,
           
          });
        }
      })
  }
  preventInput(event,valor){
    let value=valor.valor;
    if (value >= 101){
      event.preventDefault()
     valor.valor = parseInt(value.toString().substring(0,2));
    }
  }
  
  eventCheck(event,valor){
   if(event.checked){
    valor.valor="AMPARADA";
   }else{
    valor.valor="EXCLUIDO";
   }
  }
  eventNumber(event,valor){
    if(!event.checked){
     valor.valor="";
    }
 }
guardar(){
  this.detallesConfiguracion.idusuario=this.id;
  this.detallesConfiguracion.configuracion.aseguradora1.cobertura=this.detallesConfiguracion.configuracion.aseguradora1.cobertura.toString();
  this.detallesConfiguracion.configuracion.aseguradora2.cobertura=this.detallesConfiguracion.configuracion.aseguradora2.cobertura.toString();
  this.detallesConfiguracion.configuracion.aseguradora3.cobertura=this.detallesConfiguracion.configuracion.aseguradora3.cobertura.toString();
  this.detallesConfiguracion.configuracion.aseguradora1.daniosMateriales.valor=this.detallesConfiguracion.configuracion.aseguradora1.daniosMateriales.valor.toString();
  this.detallesConfiguracion.configuracion.aseguradora2.daniosMateriales.valor=this.detallesConfiguracion.configuracion.aseguradora2.daniosMateriales.valor.toString();
  this.detallesConfiguracion.configuracion.aseguradora3.daniosMateriales.valor=this.detallesConfiguracion.configuracion.aseguradora3.daniosMateriales.valor.toString();
  this.detallesConfiguracion.configuracion.aseguradora1.roboTotal.valor=this.detallesConfiguracion.configuracion.aseguradora1.roboTotal.valor.toString();
  this.detallesConfiguracion.configuracion.aseguradora2.roboTotal.valor=this.detallesConfiguracion.configuracion.aseguradora2.roboTotal.valor.toString();
  this.detallesConfiguracion.configuracion.aseguradora3.roboTotal.valor=this.detallesConfiguracion.configuracion.aseguradora3.roboTotal.valor.toString();
  this.detallesConfiguracion.configuracion.aseguradora1.responsabilidadCivilTerceros.valor=this.detallesConfiguracion.configuracion.aseguradora1.responsabilidadCivilTerceros.valor.toString();
  this.detallesConfiguracion.configuracion.aseguradora2.responsabilidadCivilTerceros.valor=this.detallesConfiguracion.configuracion.aseguradora2.responsabilidadCivilTerceros.valor.toString();
  this.detallesConfiguracion.configuracion.aseguradora3.responsabilidadCivilTerceros.valor=this.detallesConfiguracion.configuracion.aseguradora3.responsabilidadCivilTerceros.valor.toString();
  this.detallesConfiguracion.configuracion.aseguradora1.gastosMedicosOcupantes.valor=this.detallesConfiguracion.configuracion.aseguradora1.gastosMedicosOcupantes.valor.toString();
  this.detallesConfiguracion.configuracion.aseguradora2.gastosMedicosOcupantes.valor=this.detallesConfiguracion.configuracion.aseguradora2.gastosMedicosOcupantes.valor.toString();
  this.detallesConfiguracion.configuracion.aseguradora3.gastosMedicosOcupantes.valor=this.detallesConfiguracion.configuracion.aseguradora3.gastosMedicosOcupantes.valor.toString();
  this.spinner.show();

  this.ApiConfiguracion.insertConfiguracion(this.detallesConfiguracion).subscribe(res=>
    {
     if(res.exito==1){
      this.spinner.hide();
      this.copy(this.api_url+res.data.id);
      this.alerta();
      
      this.router.navigate(['']);
     }
     else{
      Swal.fire({
        icon: 'error',
        text: res.mensaje,
       
      });
     
    }
    })
}
alerta(){
  const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 1500,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })
  
  Toast.fire({
    icon: 'success',
    title: 'Se ha copiado en el portapapeles la url de la cotización'
  })
}

copy(text: string){
  this._clipboardService.copyFromContent(text)
}

getColor(value){
  if(value.seleccionado){
    return "text-success"

  }else{
    return "text-danger"
  }
}

}
