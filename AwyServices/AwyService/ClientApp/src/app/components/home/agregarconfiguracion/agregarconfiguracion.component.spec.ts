import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarconfiguracionComponent } from './agregarconfiguracion.component';

describe('AgregarconfiguracionComponent', () => {
  let component: AgregarconfiguracionComponent;
  let fixture: ComponentFixture<AgregarconfiguracionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarconfiguracionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarconfiguracionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
