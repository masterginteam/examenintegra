import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiconfiguracionService } from 'src/app/services/apiconfiguracion.service';
import { Configuracion,ConceptosConfiguracion } from 'src/app/shared/interfaces';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2/dist/sweetalert2.js';
declare var $:any;
@Component({
  selector: 'app-detalles',
  templateUrl: './detalles.component.html',
  styleUrls: ['./detalles.component.scss']
})
export class DetallesComponent implements OnInit {

  constructor(private dataApi: ApiconfiguracionService,
    private router: Router,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService) { }
   
    
public detallesConfiguracion:Configuracion= new Configuracion();


  ngOnInit() {
    this.spinner.show();
    $('[data-toggle="tooltip"]').tooltip();
    const idBook = this.route.snapshot.params['id'];
    this.dataApi.getConfiguraciones(idBook).subscribe(response=>
      {
        this.spinner.hide();
        if(response.exito===1){
          this.detallesConfiguracion = response.data;
        }else{
          Swal.fire({
            icon: 'error',
            text: response.mensaje,
           
          });
        }
      })
  }
  eventCheck(event,valor){
    if(event.checked){
     valor.valor="AMPARADA";
    }else{
     valor.valor="EXCLUIDO";
    }
 
 }
 getColor(value){
   if(value.seleccionado){
     return "text-success"
 
   }else{
     return "text-danger"
   }
 }

}
