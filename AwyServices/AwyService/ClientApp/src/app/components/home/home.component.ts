import { Component, OnInit } from '@angular/core';
import { ApiconfiguracionService } from 'src/app/services/apiconfiguracion.service';
import { Router } from '@angular/router';
import { ConfiguracionViewModel } from 'src/app/shared/interfaces';
import { DatePipe } from '@angular/common';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { ClipboardService } from 'ngx-clipboard';
import { environment } from '../../../environments/environment';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  p: number = 1;
  api_url:string = environment.urlDetalles
  constructor(private dataApi: ApiconfiguracionService,
              private router: Router,
              private spinner: NgxSpinnerService,
              private _clipboardService: ClipboardService) { }
  public configuraciones:ConfiguracionViewModel [];

  ngOnInit()  {
    this.spinner.show();
    this.dataApi.getAllConfiguraciones().subscribe(response=>
      {
        this.spinner.hide();
        if(response.exito===1){
          this.configuraciones = response.data;
          
        }else{
          Swal.fire({
            icon: 'error',
            text: response.mensaje,
           
          });
        }
      })
  }
  verDetalles(id: number){
    this.router.navigate([`configuracion/${id}`]);
  }
  alerta(){
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 1500,
      timerProgressBar: true,
      onOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })
    
    Toast.fire({
      icon: 'success',
      title: 'Se ha copiado en el portapapeles la url de la cotización'
    })
  }
 
  copy(id: number){
    this._clipboardService.copyFromContent(this.api_url+id);
    this.alerta()
  }

}
