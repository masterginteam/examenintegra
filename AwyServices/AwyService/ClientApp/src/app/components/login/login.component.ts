import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
   private _user:UserService,
   private _api:ApiService,
   private router: Router,
   private spinner: NgxSpinnerService
  ) { }
  public email: string = '';
  public password: string = '';

  ngOnInit(): void {
  }
login(){
  this.spinner.show();
  this._api.login(this.email,this.password).subscribe(response => {
    this.spinner.hide();
    if(response.exito==1){
      this.router.navigate(['']);
    }
    else{
      Swal.fire({
        icon: 'error',
        text: response.mensaje,
       
      });
    }
  })
}

}
