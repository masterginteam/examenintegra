import { TestBed } from '@angular/core/testing';

import { Api.AseguradoraService } from './api.aseguradora.service';

describe('Api.AseguradoraService', () => {
  let service: Api.AseguradoraService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Api.AseguradoraService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
