import { TestBed } from '@angular/core/testing';

import { ApiconfiguracionService } from './apiconfiguracion.service';

describe('ApiconfiguracionService', () => {
  let service: ApiconfiguracionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiconfiguracionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
