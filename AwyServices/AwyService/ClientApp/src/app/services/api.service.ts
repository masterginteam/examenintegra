import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/observable';
import {LoginResponse, Usuario} from '../shared/interfaces'
import {HttpClient} from '@angular/common/http'
import { BehaviorSubject } from 'rxjs';
import {map} from 'rxjs/operators'
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  api_url:string = environment.url+'/api/'
  public usuarioSubject: BehaviorSubject<Usuario>

  public get usuarioData():Usuario{
    return this.usuarioSubject.value;
  }

  constructor(private _http:HttpClient) {
    this.usuarioSubject= new BehaviorSubject<Usuario>(JSON.parse(localStorage.getItem('usuario')));
   }



  login(user:string , password: string): Observable<LoginResponse>{
    return this._http.post<LoginResponse>(this.api_url+`Usuario/Login`,{
      Nombre:user,
      Password :password
    }).pipe(
    map(res=> {
      if(res.exito===1){
      const user:Usuario= res.data;
      localStorage.setItem('usuario',JSON.stringify(user));
      this.usuarioSubject.next(user);
      }
      return res;
    }))
  };

  logout(){
    localStorage.removeItem('usuario');
    this.usuarioSubject.next(null);
  
  }

}

