import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { ConfiguracionesResponse, ConfiguracionResponse, Configuracion, Responseserver } from '../shared/interfaces';
import {map} from 'rxjs/operators'
import { Observable } from 'rxjs/internal/observable';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ApiconfiguracionService {
  api_url:string = environment.url+'/api/'
  constructor(private _http:HttpClient) { }

  getAllConfiguraciones(): Observable<ConfiguracionesResponse>{
    return this._http.get<ConfiguracionesResponse>(this.api_url+'configuracion')
    .pipe(
      map(res=> {        
        return res;
      }))
  }

  insertConfiguracion(oConfiguracion:Configuracion):Observable<Responseserver>{
    return this._http.post<Responseserver>(this.api_url+`Configuracion/Crear`,{
      
        Vehiculo:oConfiguracion.vehiculo,
        NombreCliente :oConfiguracion.usuario,
        Configuracion: oConfiguracion.configuracion,
        IdUsuario:parseInt(oConfiguracion.idusuario.toString()),
        IdAseguradora:parseInt(oConfiguracion.idaseguradora.toString())
    }).pipe(
    map(res=> {
      return res;
    }))
  }

  getConfiguraciones(id:number): Observable<ConfiguracionResponse>{
    return this._http.get<ConfiguracionResponse>(this.api_url+'configuracion/'+id)
    .pipe(
      map(res=> {        
        return res;
      }))
  }
}
