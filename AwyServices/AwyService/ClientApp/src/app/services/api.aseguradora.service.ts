import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {  AseguradoraResponse } from '../shared/interfaces';
import {map} from 'rxjs/operators'
import { Observable } from 'rxjs/internal/observable';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ApiAseguradoraService {
  api_url:string = environment.url+'/api/'
  constructor(private _http:HttpClient) { }

  getAllAseguradoras(): Observable<AseguradoraResponse>{
    return this._http.get<AseguradoraResponse>(this.api_url+'Aseguradora')
    .pipe(
      map(res=> {        
        return res;
      }))
  }


}