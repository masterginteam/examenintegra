import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import {AuthGuard} from './Seguridad/auth.guard'
import { DetallesComponent } from './components/home/verdetalles/detalles.component';
import { AgregarconfiguracionComponent } from './components/Home/agregarconfiguracion/agregarconfiguracion.component';

const routes: Routes = [
  
  { path: '', component: HomeComponent ,canActivate:[AuthGuard]},
  { path: 'configuracion/:id', component: DetallesComponent },
  { path: 'agregarConfiguracion', component: AgregarconfiguracionComponent ,canActivate:[AuthGuard]},
  { path: 'login', component: LoginComponent },

]; 

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}