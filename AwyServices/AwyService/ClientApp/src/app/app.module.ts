import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './components/app/app.component';
import { HeaderComponent } from './components/header/header.component';
import { LoginComponent } from './components/login/login.component';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http'
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './components/home/home.component';
import { TwsInterceptorService } from './Seguridad/jws-interceptor.service';
import { DetallesComponent } from './components/home/verdetalles/detalles.component';
import { AgregarconfiguracionComponent } from './components/Home/agregarconfiguracion/agregarconfiguracion.component';
import { RegistrarComponent } from './components/login/registrar/registrar.component';
import { NgxSpinnerModule } from "ngx-spinner";
import {NgxPaginationModule} from 'ngx-pagination';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClipboardModule } from 'ngx-clipboard';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    HomeComponent,
    DetallesComponent,
    AgregarconfiguracionComponent,
    RegistrarComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    NgxSpinnerModule,
    NgxPaginationModule,
    BrowserAnimationsModule,
    ClipboardModule
  ],
  providers: [ {provide:HTTP_INTERCEPTORS,useClass:TwsInterceptorService, multi:true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
