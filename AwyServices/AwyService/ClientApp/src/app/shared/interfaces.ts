export interface LoginResponse{
    exito:number,
    mensaje:string,
    data: Usuario
}
export interface Responseserver{
    exito:number,
    mensaje:string,
    data: any
}

export interface Usuario{
    id :number
    nombre :string
    apellidoMaterno :string
    apellidoPaterno :string
    email :string,
    token:string,
    expira:any

}

export interface ConfiguracionesResponse{
    exito:number,
    mensaje:string,
    data: ConfiguracionViewModel[]
}
export interface AseguradoraResponse{
    exito:number,
    mensaje:string,
    data: Aseguradora[]
}


export interface ConfiguracionViewModel{
    id :number
    vehiculo :string
    nombreCliente :string
    fechaCreacion :Date
    idUsuario :string

}

export interface ConfiguracionResponse{
    exito:number,
    mensaje:string,
    data: Configuracion
}
export class Configuracion {

    constructor() { }
    id:number
    vehiculo:string
    usuario:string
    idaseguradora:number
    aseguradora:string
    idusuario:number
    configuracion:DetalleConfiguracion=  new DetalleConfiguracion();
    fechaCreacion:Date
  }
  export class Aseguradora {

    constructor() { }
    id:number
    nombre:string
  
  }



export class DetalleConfiguracion{
    constructor() { }
     aseguradora1:ConceptosConfiguracion =  new ConceptosConfiguracion();
     aseguradora2:ConceptosConfiguracion= new ConceptosConfiguracion();
     aseguradora3:ConceptosConfiguracion= new ConceptosConfiguracion();
    }

export class ConceptosConfiguracion{
    constructor() {
        this.daniosMateriales.valor='';
        this.roboTotal.valor='';
        this.responsabilidadCivilTerceros.valor='';
        this.gastosMedicosOcupantes.valor='';
     }
    cobertura:string
    daniosMateriales:ValoresConfiguracionString = new ValoresConfiguracionString();
    noPagoDeduciblePerdidaParcial:ValoresConfiguracionString= new ValoresConfiguracionString();
    noPagoDeduciblePerdidaTotal:ValoresConfiguracionString= new ValoresConfiguracionString();
    roboTotal:ValoresConfiguracionString= new ValoresConfiguracionString();
    responsabilidadCivilTerceros:ValoresConfiguracionString= new ValoresConfiguracionString();
    gastosMedicosOcupantes:ValoresConfiguracionString= new ValoresConfiguracionString();
    asistenciaVial:ValoresConfiguracionString= new ValoresConfiguracionString();
    defensaLegal:ValoresConfiguracionString= new ValoresConfiguracionString();
    responsabilidadCivilFallecimiento:ValoresConfiguracionString= new ValoresConfiguracionString();
    responsabilidadCivilEUA:ValoresConfiguracionString= new ValoresConfiguracionString();
    responsabilidadCivilOcupantes:ValoresConfiguracionString= new ValoresConfiguracionString();
    muerteAccidentaConductor:ValoresConfiguracionString= new ValoresConfiguracionString();
    autoSustituto:ValoresConfiguracionString= new ValoresConfiguracionString();
    ultimosGastosOcupantes:ValoresConfiguracionString= new ValoresConfiguracionString();
    extensionCoberturas:ValoresConfiguracionString= new ValoresConfiguracionString();
    llantasRines:ValoresConfiguracionString= new ValoresConfiguracionString();
    roboParcial:ValoresConfiguracionString= new ValoresConfiguracionString();
    espejosFaros:ValoresConfiguracionString= new ValoresConfiguracionString();
    multasCorralones:ValoresConfiguracionString= new ValoresConfiguracionString();
    desbielamiento:ValoresConfiguracionString= new ValoresConfiguracionString();
    reparacionAgencia:ValoresConfiguracionString= new ValoresConfiguracionString();
    asistenciaServiciosFunerarios:ValoresConfiguracionString= new ValoresConfiguracionString();
    reposiscionLlaves:ValoresConfiguracionString= new ValoresConfiguracionString();
    autoMasNuevo:ValoresConfiguracionString= new ValoresConfiguracionString();
    responsabilidadCivilFamiliar:ValoresConfiguracionString= new ValoresConfiguracionString();
    roboContenidos:ValoresConfiguracionString= new ValoresConfiguracionString();
    equipoEspecial:ValoresConfiguracionString= new ValoresConfiguracionString();
    responsabilidadCivilRemolques:ValoresConfiguracionString= new ValoresConfiguracionString();
    adaptacionConversiones:ValoresConfiguracionString= new ValoresConfiguracionString();
    responsabilidadCivilAdaptacionConver:ValoresConfiguracionString= new ValoresConfiguracionString();
}


export class ValoresConfiguracionString{
    constructor() {
        this.seleccionado=false; 
        this.valor= 'EXCLUIDO';
     }
    seleccionado:boolean
    valor:string
}