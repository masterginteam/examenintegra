import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { ApiService } from '../services/api.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
constructor(private router:Router,
            private apiService:ApiService){

}

  canActivate(route:ActivatedRouteSnapshot){
    const usuario= this.apiService.usuarioData;
    if(usuario){
      return true
    }
this.router.navigate(['login/'])
    return false;
  }
   

  
}
