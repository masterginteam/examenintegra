import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { NgxSpinnerService } from "ngx-spinner";

@Injectable({
  providedIn: 'root'
})
export class TwsInterceptorService  implements HttpInterceptor {

  constructor(private apiservice: ApiService,private router: Router,private spinner: NgxSpinnerService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      const usuario = this.apiservice.usuarioData;
      if (usuario) {
          request = request.clone({
              setHeaders: {
                  Authorization: `Bearer ${usuario.token}`
              }
          });
      }
      return next.handle(request).pipe(
        catchError((err: HttpErrorResponse) => {
          this.spinner.hide();
          if (err.status === 401) {
            this.apiservice.logout();
           
            this.router.navigateByUrl('/login');
            
          }
  
          return throwError( err );
  
        })
      );
  }
}
