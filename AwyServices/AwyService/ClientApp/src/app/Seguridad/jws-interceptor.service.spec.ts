import { TestBed } from '@angular/core/testing';

import { TwsInterceptorService } from './jws-interceptor.service';

describe('TwsInterceptorService', () => {
  let service: TwsInterceptorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TwsInterceptorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
