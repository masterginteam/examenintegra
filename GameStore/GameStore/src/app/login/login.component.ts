import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthService } from '../services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  sending = false;
  submit = false;
  returnUrl: string;
  userObj: any = {};
  messageError = "";

  constructor(
    private formBuilder: FormBuilder,
    private aroute: ActivatedRoute,
    private router: Router,
    private authService: AuthService) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.authService.logout();
    this.returnUrl = this.aroute.snapshot.queryParams['returnUrl'];
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submit = true;
    this.messageError = "";

    if (this.loginForm.invalid) {
      return;
    }
    this.sending = true;
    this.authService.login(this.f.username.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          this.userObj = this.authService.authTokenIsValid();
          if (this.returnUrl) {
            this.router.navigate([this.returnUrl]);
          } else
            this.router.navigate(["/admin/" + this.userObj.Email]);
        },
        error => {
          if (error && error.status === 400)
            this.messageError = 'Incorrect username or password';
          else
            this.messageError = "Internal server error";
          this.sending = false;
        });
  }
}
