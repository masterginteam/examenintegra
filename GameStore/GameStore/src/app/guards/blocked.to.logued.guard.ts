﻿import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CONST } from '../CONST';
import { AuthService } from '../services';

@Injectable()
export class BlockedToLoguedGuard implements CanActivate {

  constructor(
    private router: Router,
    private authService: AuthService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    var a = localStorage.getItem(CONST.TOKEN_KEY);
    if (localStorage.getItem(CONST.TOKEN_KEY)) {      
      var userLoguedObj = this.authService.authTokenIsValid();
      if (userLoguedObj && userLoguedObj.Email)
        this.router.navigate(['/admin/' + userLoguedObj.Email]);
      else
        this.router.navigate(['/']);
      return false;
    } else {
      return true;
    }
  }
}
