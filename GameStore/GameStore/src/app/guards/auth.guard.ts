﻿import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../services';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private authService: AuthService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const userLoguedObj = this.authService.authTokenIsValid();
    if (userLoguedObj && userLoguedObj.Role === "Admin")
      return true;
    else if (userLoguedObj && userLoguedObj.Role !== "Admin") {
      this.router.navigate(['/']);
      return false;
    } else {
      // not logged in so redirect to login page with the return url
      this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
      return false;
    }

  }
}
