﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CONST } from '../CONST';

@Injectable()
export class GameService {
  constructor(private http: HttpClient) { }

  getRegions() {   
    return this.http.get(`${CONST.API_URL}/regions`);
  }

  getConsolesByRegion(region) {
    return this.http.get(`${CONST.API_URL}/consolas/getConsolesByRegion/` + region);
  }

  saveVideoGame(data) {
    //It needs improve
    return this.http.post<any>(`${CONST.API_URL}/videogames/saveVideoGame`, {
      "clasification": data.clasification,
      "description": data.description,
      "region": data.region,
      "videoGameName": data.videoGameName,
      "stock": parseInt(data.stock),
      "price": parseInt(data.price),
    });
  }

  searchVideoGames(data) {   
    return this.http.post<any>(`${CONST.API_URL}/videogames/searchVideoGames`, data);
  }
}
