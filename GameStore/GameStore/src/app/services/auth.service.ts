﻿import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpClient } from '@angular/common/http';
import { CONST } from '../CONST';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  public loguedUser: any;

  constructor(
    private router: Router,
    private http: HttpClient,
    private authHelper: JwtHelperService,
  ) { }

  login(username: string, password: string) {
    return this.http.post<any>(`${CONST.API_URL}/Account/token`, { email: username, password: password })
      .pipe(map(user => {
        if (user && user.token) {
          localStorage.setItem(CONST.TOKEN_KEY, JSON.stringify(user));
        }
        return user;
      }));
  }

  authTokenIsValid() {
    let token = window.localStorage.getItem(CONST.TOKEN_KEY);
    if (token) {
      let decoded = this.authHelper.decodeToken(token);
      let isExpired = this.authHelper.isTokenExpired(token);
      if (!isExpired) {
        this.loguedUser = decoded;
        this.loguedUser.token = token;
        return this.loguedUser;
      } else {
        return null;
      }
    } else
      return null;
  }

  logout(redirect?: string) {
    window.localStorage.removeItem(CONST.TOKEN_KEY);
    if (redirect) {
      this.router.navigate([redirect]);
      location.reload();
    }
  }
}
