import { Component } from '@angular/core';
import { AuthService } from './services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {  
  public loguedUser: any = {};

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  changeOfRoutes() {
    this.loguedUser = this.authService.authTokenIsValid();    
  }
  
  logOut() {    
    this.authService.logout("/");
  }
}
