import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { Page404Component } from './page404/page404.component';
import { AdminComponent } from "./admin/admin.component";
import { AuthGuard, BlockedToLoguedGuard } from './guards';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'games/:page', component: HomeComponent },
  { path: 'login', component: LoginComponent, canActivate: [BlockedToLoguedGuard] },
  { path: 'admin/:username', component: AdminComponent, canActivate: [AuthGuard] },
  { path: '**', component: Page404Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }