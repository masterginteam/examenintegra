import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthService, GameService } from '../services';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  gameForm: FormGroup;
  sending = false;
  submit = false;
  returnUrl: string;
  userObj: any = {};
  messageError = "";
  regionList = [];
  consoleList = [];
  isNotNumeric = false;

  constructor(
    private formBuilder: FormBuilder,
    private aroute: ActivatedRoute,
    private router: Router,
    private gameService: GameService,
    private authService: AuthService) { }

  ngOnInit(): void {
    this.gameForm = this.formBuilder.group({
      videoGameName: ['', Validators.required],
      clasification: ['', Validators.required],
      price: ['', Validators.required],      
      region: ['', Validators.required],
      console: ['', Validators.required],
      stock: ['', Validators.required],
      description: ['']
    });
    this.loadRegions();
  }
  get f() { return this.gameForm.controls; }

  loadRegions() {
    this.gameService.getRegions()
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.regionList = data;
        },
        error => {
          this.messageError = "Internal server error";
        });
  }

  onSave() {
    this.submit = true;
    this.messageError = "";
    this.isNotNumeric = false;

    if (this.gameForm.invalid) {
      return;
    }

    if (!this.IsNumeric(this.gameForm.value.price)) {
      this.isNotNumeric = true;
      return;
    }

    this.sending = true;
    this.gameService.saveVideoGame(this.gameForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(['/']);
        },
        error => {
          this.sending = false;
          this.messageError = 'Internal server error';
        });
  }

  IsNumeric(input) {
    return (input - 0) == input && ('' + input).trim().length > 0;
  }

  changeRegion(e) {
    var region = this.gameForm.get('region');
    this.loadConsolesByRegion(region.value);
  }

  loadConsolesByRegion(region) {
    this.gameService.getConsolesByRegion(region)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.consoleList = data.list;
        },
        error => {
          this.messageError = 'Internal server error';
        });
  }

  public handleError = (controlName: string, errorName: string) => {
    return this.gameForm.controls[controlName].hasError(errorName);
  }

}