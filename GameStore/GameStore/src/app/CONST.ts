export const CONST = {
  API_URL: 'https://localhost:5001/api',
  TOKEN_KEY: 'currentUser',
  STORAGE: 'https://res.cloudinary.com/shimozurdo/image/upload/',
}
