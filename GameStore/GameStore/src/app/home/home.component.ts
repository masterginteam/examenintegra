import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { GameService } from '../services';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private gameService: GameService,
    private aroute: ActivatedRoute,
    private router: Router) { }

  query = "";
  filterList = ["Console", "Region", "Clasification", "Stock"];
  filterItem = "Chose a filter";
  videoGamesList = [];
  messageError = "";
  noPage = 1;

  ngOnInit(): void {
    const allParams = this.aroute.snapshot.params;
    if (allParams && allParams.page)
      this.noPage = allParams.page;
    this.search();
  }

  typingSearch(e) {
    this.search();
  };

  changeFilter(e) {
    if (this.filterItem !== "Chose a filter")
      this.search();
  }

  search() {
    this.gameService.searchVideoGames({
      page: this.noPage,
      limit: 3,
      q: this.query,
      item: this.filterItem
    })
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.videoGamesList = data;
        },
        error => {
          this.messageError = "Internal server error";
        });
  }

  changePage(isNext) {

    if (isNext)
      this.noPage += 1;
    else if (!isNext) {
      this.noPage -= 1;
      if (this.noPage <= 0)
        this.noPage = 1
    }
    this.search();
  }
}
