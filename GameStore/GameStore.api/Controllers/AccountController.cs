﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using GameStore.Data;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System;
using GameStore.api.Models;
using GameStore.Models;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using System.Linq;

namespace GameStore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        public IConfiguration _configuration;
        private readonly ApplicationDbContext _context;

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IConfiguration config,
            ApplicationDbContext context,
            RoleManager<IdentityRole> roleManager)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = config;
            _context = context;
        }

        /// <summary>
        /// Entry point token based authentication
        /// </summary>
        /// <param name="_userData">Receive an Email and Password</param>
        /// <returns>Jwt Token</returns>
        [HttpPost("Token")]
        public async Task<IActionResult> Post(UserViewModel _userData)
        {

            var result = await _signInManager.PasswordSignInAsync(_userData.Email, _userData.Password, false, lockoutOnFailure: false);
            if (result.Succeeded)
            {
                var user = await _signInManager.UserManager.FindByNameAsync(_userData.Email);
                var role = (await _userManager.GetRolesAsync(user)).FirstOrDefault();
                var claims = new[] {
                     new Claim(JwtRegisteredClaimNames.Sub, _configuration["Jwt:Subject"]),
                     new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                     new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                     new Claim("Role",role),
                     new Claim("Id", user.Id.ToString()),
                     new Claim("Name", user.UserName),
                     new Claim("Email", user.Email),
                    };

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
                var signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                var token = new JwtSecurityToken(_configuration["Jwt:Issuer"], _configuration["Jwt:Audience"], claims, expires: DateTime.UtcNow.AddDays(1), signingCredentials: signIn);

                return Json(new { token = new JwtSecurityTokenHandler().WriteToken(token) });
            }
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Registered a new user in the Data base
        /// </summary>
        /// <param name="_userData"></param>
        /// <returns></returns>
        [HttpPost("Register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody] UserViewModel _userData)
        {
            var user = new ApplicationUser { UserName = _userData.Email, Email = _userData.Email, EmailConfirmed = true };
            var result = await _userManager.CreateAsync(user, _userData.Password);
            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, "Customer");
                await _signInManager.SignInAsync(user, isPersistent: false);
                return Json(new { value = user.Id });
            }

            return BadRequest();
        }
    }
}