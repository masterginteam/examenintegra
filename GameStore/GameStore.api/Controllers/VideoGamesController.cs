﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GameStore.Data;
using GameStore.Models;
using GameStore.api.Models;
using System;

namespace GameStore.api.Controllers
{
    //TO DO Authorization
    [Route("api/[controller]")]
    [ApiController]
    public class VideoGamesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public VideoGamesController(ApplicationDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get all videogames
        /// </summary>
        /// <returns></returns>
        // GET: api/VideoGames
        [HttpGet]
        public async Task<ActionResult<IEnumerable<VideoGame>>> GetVideoGames()
        {
            return await _context.VideoGames.ToListAsync();
        }

        /// <summary>
        /// Save new game and its relation with the table regionGame
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        // POST: api/saveVideoGame        
        [HttpPost("saveVideoGame")]
        public async Task<ActionResult<VideoGame>> SaveVideoGame(VideoGameViewModel data)
        {
            var region = _context.Regions.FirstOrDefault(vm => vm.Name == data.Region);

            var videoGameId = Guid.NewGuid();

            var videoGame = new VideoGame
            {
                Id = videoGameId.ToString(),
                Name = data.VideoGameName,
                Clasification = data.Clasification,
                Description = data.Description,
                Amount = data.Stock,
                Price = data.Price,
            };

            _context.Add<VideoGame>(videoGame);

            var regionGame = new RegionGame
            {
                Id = Guid.NewGuid().ToString(),
                RegionId = region.Id.ToString(),
                VideoGameId = videoGameId.ToString(),
            };

            _context.Add<RegionGame>(regionGame);

            await _context.SaveChangesAsync();

            return Ok();
        }


        /// <summary>
        /// Find All Games using pagination
        /// </summary>
        /// <param name="region">name region</param>
        /// <returns>Consoles by</returns>
        // GET: api/getConsolesByRegion
        [HttpPost("searchVideoGames")]
        public async Task<ActionResult<IEnumerable<Consola>>> SearchVideoGames(VideoGameViewModel data)
        {
            var collection = (from videoGames in _context.VideoGames
                              join regionGames in _context.RegionGames on videoGames.Id equals regionGames.VideoGameId
                              join regions in _context.Regions on regionGames.RegionId equals regions.Id
                              join consolas in _context.Consolas on regions.Id equals consolas.RegionId
                              where (videoGames.Name.ToLower().Contains(data.Q)
                              || videoGames.Clasification.ToLower().Contains(data.Q)
                              || videoGames.Description.ToLower().Contains(data.Q)
                              || videoGames.Amount.ToString().ToLower().Contains(data.Q)
                              || videoGames.Price.ToString().ToLower().Contains(data.Q)
                              || consolas.Name.ToLower().Contains(data.Q)
                              || regions.Name.ToLower().Contains(data.Q)
                              )

                              orderby videoGames.Name

                              select new
                              {
                                  videoGames.Id,
                                  videoGameName = videoGames.Name,
                                  videoGames.Description,
                                  videoGames.Clasification,
                                  videoGames.Price,
                                  stock = videoGames.Amount,
                                  regionName = regions.Name,
                                  consoleName = consolas.Name
                              }
                              );

            var offset = (data.Page - 1) * data.Limit;
            var result = collection.Distinct().Skip(offset).Take(data.Limit).ToList();            

            return Json(result);
            
        }
    }
}

