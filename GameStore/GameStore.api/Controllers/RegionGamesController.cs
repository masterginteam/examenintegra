﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GameStore.Data;
using GameStore.Models;

namespace GameStore.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegionGamesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public RegionGamesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/RegionGames
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RegionGame>>> GetRegionGames()
        {
            return await _context.RegionGames.ToListAsync();
        }

        // GET: api/RegionGames/5
        [HttpGet("{id}")]
        public async Task<ActionResult<RegionGame>> GetRegionGame(string id)
        {
            var regionGame = await _context.RegionGames.FindAsync(id);

            if (regionGame == null)
            {
                return NotFound();
            }

            return regionGame;
        }

        // PUT: api/RegionGames/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRegionGame(string id, RegionGame regionGame)
        {
            if (id != regionGame.Id)
            {
                return BadRequest();
            }

            _context.Entry(regionGame).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RegionGameExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/RegionGames
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<RegionGame>> PostRegionGame(RegionGame regionGame)
        {
            _context.RegionGames.Add(regionGame);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (RegionGameExists(regionGame.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetRegionGame", new { id = regionGame.Id }, regionGame);
        }

        // DELETE: api/RegionGames/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<RegionGame>> DeleteRegionGame(string id)
        {
            var regionGame = await _context.RegionGames.FindAsync(id);
            if (regionGame == null)
            {
                return NotFound();
            }

            _context.RegionGames.Remove(regionGame);
            await _context.SaveChangesAsync();

            return regionGame;
        }

        private bool RegionGameExists(string id)
        {
            return _context.RegionGames.Any(e => e.Id == id);
        }
    }
}
