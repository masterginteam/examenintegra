﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GameStore.Models;
using GameStore.Data;

namespace GameStore.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConsolasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ConsolasController(ApplicationDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Find All consoles by a region
        /// </summary>
        /// <param name="region">name region</param>
        /// <returns>Consoles by</returns>
        // GET: api/getConsolesByRegion
        [HttpGet("getConsolesByRegion/{region?}")]
        public async Task<ActionResult<IEnumerable<Consola>>> GetConsolesByRegion(string region = "")
        {
            var data = _context.Regions
        .Join(
            _context.Consolas,
            region => region.Id,
            consola => consola.Region.Id,
            (region, consola) => new
            {
                ConsolaId = consola.Id,
                ConsolaName = consola.Name,
                RegionName = region.Name,
                RegionId = region.Id
            }
        ).Where(s => s.RegionName == region).ToList();

            var consolesList = new List<dynamic>();

            foreach (var item in data)
                consolesList.Add(new
                {
                    item.ConsolaId,
                    item.ConsolaName,
                    item.RegionName,
                    item.RegionId
                });
            if (consolesList == null)
                return NotFound();
            return Json(new { list = consolesList });
        }        
    }
}
