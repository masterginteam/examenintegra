﻿using IdentityServer4.EntityFramework.Options;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using GameStore.Models;

namespace GameStore.Data
{
    /// <summary>
    /// Creates an application db context usinng data base code first
    /// </summary>
    public class ApplicationDbContext : ApiAuthorizationDbContext<ApplicationUser>
    {
        public ApplicationDbContext(
            DbContextOptions options,
            IOptions<OperationalStoreOptions> operationalStoreOptions) : base(options, operationalStoreOptions)
        {
        }
        public DbSet<VideoGame> VideoGames { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<Consola> Consolas { get; set; }
        public DbSet<PurchaseOrder> PurchaseOrders { get; set; }        
        public DbSet<RegionGame> RegionGames { get; set; }
        
    }
}
