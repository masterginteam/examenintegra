﻿using Microsoft.AspNetCore.Identity;

namespace GameStore.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string FatherLastName { get; set; }
        public string MotherLastName { get; set; }
    }
}
