﻿namespace GameStore.Models
{
    public partial class Consola
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string UrlImage { get; set; }
        public string RegionId { get; set; }
        public virtual Region Region { get; set; }
    }
}
