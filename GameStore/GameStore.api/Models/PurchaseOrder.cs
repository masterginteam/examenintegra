﻿using GameStore.Models;
using System;

namespace GameStore.Models
{
    public partial class PurchaseOrder
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public int Amount { get; set; }
        public DateTime Date { get; set; }
        public decimal Total { get; set; }
        public string AspNetUsersId { get; set; }
        public string VideoGameId { get; set; }
        public virtual ApplicationUser AspNetUsers { get; set; }
        public virtual VideoGame VideoGame { get; set; }
    }
}
