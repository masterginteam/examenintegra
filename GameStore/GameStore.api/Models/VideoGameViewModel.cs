﻿namespace GameStore.api.Models
{
    public class VideoGameViewModel
    {
        public string Region { get; set; }
        public string VideoGameName { get; set; }
        public string Clasification { get; set; }
        public string Description { get; set; }
        public int Stock { get; set; }
        public string Console { get; set; }
        public int Page { get; set; }
        public decimal Price { get; set; }
        public int Limit { get; set; }
        public string Q { get; set; }
        public string Filter { get; set; }
    }
}
