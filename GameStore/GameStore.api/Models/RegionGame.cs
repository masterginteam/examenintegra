﻿namespace GameStore.Models
{
    public partial class RegionGame
    {
        public string Id { get; set; }
        public string RegionId { get; set; }
        public string VideoGameId { get; set; }
        public virtual Region Region { get; set; }
        public virtual VideoGame VideoGame { get; set; }
    }
}
