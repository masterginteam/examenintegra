﻿namespace GameStore.Models
{
    public partial class Region
    {
        public string Id { get; set; }
        public string Name { get; set; }       
        public string Description { get; set; }
    }
}
