﻿namespace GameStore.Models
{
    public partial class VideoGame
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Clasification { get; set; }
        public string Description { get; set; }
        public int Amount { get; set; }
        public decimal Price { get; set; }         
        public string UrlImage { get; set; }
    }
}
